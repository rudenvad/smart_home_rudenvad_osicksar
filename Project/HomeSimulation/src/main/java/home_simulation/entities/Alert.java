package home_simulation.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of actor or item notifications, warning, alerts..
 */
public class Alert
{
    private Integer time;
    private String sourceName;
    private String info;

    private static List<Alert> alerts = new ArrayList();

    private Alert(Integer time, String sourceName, String info)
    {
        this.time = time;
        this.sourceName = sourceName;
        this.info = info;
    }

    @Override
    public String toString() { return "[Alert]" + "[" + this.time + "]" + "[" + this.sourceName + "] -> " + this.info; }

    public static void GenerateAlert(Integer time, String sourceName, String info) { alerts.add(new Alert(time, sourceName, info)); }

    public static List<Alert> getAlerts() { return alerts; }
}
