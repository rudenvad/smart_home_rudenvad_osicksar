package home_simulation.entities;

import home_simulation.enums.ActionType;

/**
 * Class representation of action occurred by actor
 */
public class Action
{
    private int minute;
    private ActionType type;
    private String actor;
    private String item;

    public Action(int minute, ActionType type, String actor, String item)
    {
        setMinute(minute);
        setType(type);
        setActor(actor);
        setItem(item);
    }

    //region getters and setters
    public int getMinute() { return minute; }
    public void setMinute(int minute) { this.minute = minute; }

    public ActionType getType() { return type; }
    public void setType(ActionType type) { this.type = type; }

    public String getActor() { return actor; }
    public void setActor(String actor) { this.actor = actor; }

    public String getItem() { return item; }
    public void setItem(String item) { this.item = item; }
    //endregion
}
