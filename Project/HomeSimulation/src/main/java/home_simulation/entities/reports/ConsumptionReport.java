package home_simulation.entities.reports;

import home_simulation.enums.ConsumableType;

import java.util.Map;

/**
 * Class converting consumption data to json report
 */
public class ConsumptionReport
{
    //How many times item was used
    private Integer totalUsageTime;
    //How much consumable did it use
    private Map<ConsumableType, Double> consumableValue;
    //How much it cost
    private Map<ConsumableType,Double> consumableCost;

    public ConsumptionReport(Integer occupationTime, Map<ConsumableType, Double> consumableValue, Map<ConsumableType,Double> consumableCost)
    {
        this.totalUsageTime = occupationTime;
        this.consumableValue = consumableValue;
        this.consumableCost = consumableCost;
    }

    //region getters and setters for json
    public Integer getTotalUsageTime() { return totalUsageTime; }
    public void setTotalUsageTime(Integer totalUsageTime) { this.totalUsageTime = totalUsageTime; }
    public Map<ConsumableType, Double> getConsumableCost() { return consumableCost; }
    public void setConsumableCost(Map<ConsumableType, Double> consumableCost) { this.consumableCost = consumableCost; }
    public Map<ConsumableType, Double> getConsumableValue() { return consumableValue; }
    public void setConsumableValue(Map<ConsumableType, Double> consumableValue) { this.consumableValue = consumableValue; }
    //endregion
}