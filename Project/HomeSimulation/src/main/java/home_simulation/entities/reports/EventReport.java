package home_simulation.entities.reports;

import java.util.List;

/**
 * Class converting event data to json report
 */
public class EventReport
{
    //Time when event was published
    private Integer minute;
    //List of all event handlers
    private List<String> handlers;

    public EventReport(Integer minute, List<String> handlers)
    {
        this.minute = minute;
        this.handlers = handlers;
    }

    //region getters and setters for json
    public Integer getMinute() { return minute; }
    public void setMinute(Integer minute) { this.minute = minute; }
    public List<String> getHandlers() { return handlers; }
    public void setHandlers(List<String> handlers) { this.handlers = handlers; }
    //endregion
}