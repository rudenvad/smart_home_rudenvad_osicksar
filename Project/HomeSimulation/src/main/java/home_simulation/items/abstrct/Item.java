package home_simulation.items.abstrct;

import home_simulation.actors.abstrct.Actor;
import home_simulation.entities.Alert;
import home_simulation.entities.Event;
import home_simulation.simulation.SimulationClock;
import home_simulation.Utils.Utils;
import home_simulation.enums.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public abstract class Item
{
    protected String name;
    protected Room room;
    protected int floor;
    protected boolean occupied;
    protected List<Event> events;
    protected SimulationClock clock;

    protected Map<EventType, List<Actor>> eventHandlers;

    protected Map<ConsumableType, Double> consumables;
    protected int degradation = 0;
    protected int totalOccupationTime;

    protected abstract List<EventType> CustomGetPossibleEventTypes();
    protected abstract void Consumption();
    protected abstract void CustomUpdate();
    public abstract List<ActionType> GetPossibleActions();
    public abstract boolean IsSporty();
    public abstract void ExecuteAction(ActionType actionType);

    public Item(String name, Room room, int floor)
    {
        if(Utils.IsNullOrWhiteSpace(name))
        {
            throw new IllegalArgumentException("Constructor parameters can't be null, empty or blank!");
        }

        setName(name);
        setRoom(room);
        setFloor(floor);

        setEvents(new ArrayList<Event>());
        setEventHandlers(new HashMap<EventType, List<Actor>>());
        setConsumables(new HashMap<ConsumableType,Double>());

        for(var eventType : GetPossibleEventTypes())
        {
            this.eventHandlers.put(eventType, new ArrayList<Actor>());
        }
    }

    public List<EventType> GetPossibleEventTypes()
    {
        var eventTypes = CustomGetPossibleEventTypes();
        eventTypes.add(EventType.Broken);
        return eventTypes;
    }

    public void Subscribe(EventType eventType, List<Actor> actors) { this.eventHandlers.get(eventType).addAll(actors); }

    public void Update()
    {
        if(this.isOccupied())
        {
            this.totalOccupationTime +=1;
        }

        Degradation();

        var rand = ThreadLocalRandom.current().nextInt(0, 100) % 25 == 0;
        if(rand && this.getConsumables().size() > 0)
        {
            Alert.GenerateAlert(this.clock.getMinute(), this.name, "My consumption of " + ConsumableType.Electricity + " is " + String.format("%.2f",this.consumables.get(ConsumableType.Electricity)));
        }

        Consumption();

        CustomUpdate();
    }

    protected void Publish(EventType eventType)
    {
        if(this.eventHandlers.get(eventType) == null)
        {
            this.eventHandlers.put(eventType, new ArrayList<Actor>());
        }

        for(var handler : this.eventHandlers.get(eventType))
        {
            if(handler.getState() != ActorState.UsingSportItem)
            {
                handler.HandleEvent(this, eventType);
            }
        }

        this.events.add(new Event(
            this.clock.getMinute(),
            eventType,
            this.name,
            this.eventHandlers.get(eventType).stream()
                    .filter(h -> h.getState() != ActorState.UsingSportItem)
                    .map(Actor::getName)
                    .collect(Collectors.toList())
        ));
    }

    protected void Degradation()
    {
        var value = 1;
        if(this.isOccupied()) { value = 2; }
        this.degradation += value;
    }

    //region getters and setters
    public String getName() { return name; }
    protected void setName(String name) { this.name = name; }

    public Room getRoom() { return room; }
    protected void setRoom(Room room) { this.room = room; }

    public int getFloor() { return floor; }
    protected void setFloor(int floor) { this.floor = floor; }

    public boolean isOccupied() { return occupied; }
    public void setOccupied(boolean occupied) { this.occupied = occupied; }

    public List<Event> getEvents() { return events; }
    protected void setEvents(List<Event> events) { this.events = events; }

    public Map<EventType, List<Actor>> getEventHandlers() { return eventHandlers; }
    public void setEventHandlers(Map<EventType, List<Actor>> eventHandlers) { this.eventHandlers = eventHandlers; }

    public SimulationClock getClock() { return clock; }
    public void setClock(SimulationClock clock) { this.clock = clock; }

    public int getDegradation() { return degradation; }
    protected void setDegradation(int degradation) { this.degradation = degradation; }

    public Map<ConsumableType, Double> getConsumables() { return consumables; }
    protected void setConsumables(Map<ConsumableType, Double> consumables) { this.consumables = consumables; }

    public int getTotalOccupationTime() { return totalOccupationTime; }
    //endregion
}
