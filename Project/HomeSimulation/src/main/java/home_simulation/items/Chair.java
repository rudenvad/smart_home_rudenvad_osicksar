package home_simulation.items;

import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.api.IBrokenable;
import home_simulation.enums.ActionType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Chair extends Item implements IBrokenable
{
    private boolean broken = false;
    private boolean dirty = false;

    public Chair(String name, Room room, int floor) { super(name, room, floor); }

    @Override
    public void Broke()
    {
        setBroken(true);
        Publish(EventType.Broken);
    }

    @Override
    public void Repair()
    {
        setDegradation(0);
        setBroken(false);
    }

    @Override public boolean IsBroken() { return this.broken; }

    @Override public boolean IsSporty() { return false; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        var eventTypes = new ArrayList<EventType>();

        eventTypes.add(EventType.ChairIsDirty);

        return eventTypes;
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();

        if(IsBroken())
        {
            actions.add(ActionType.Repair);
            return actions;
        }

        if(isDirty())
        {
            actions.add(ActionType.ChairClear);
            return actions;
        }

        actions.add(ActionType.ChairSitDown);

        return actions;
    }

    @Override
    public void ExecuteAction(ActionType actionType)
    {
        switch (actionType)
        {
            case Repair: Repair();break;
            case ChairSitDown: SitDown();break;
            case ChairClear: ChairIsDirty();break;
            default: break;
        }
    }

    @Override
    protected void CustomUpdate()
    {
        var rand = ThreadLocalRandom.current().nextInt(0, 100) % 4 == 0;
        if(getDegradation() >= 50 && rand && !IsBroken())
        {
            Broke();
        }

        var shouldBeDirty = !isDirty() && ThreadLocalRandom.current().nextInt(0, 100) % 5 == 0;
        if(shouldBeDirty)
        {
            setDirty(true);
            Publish(EventType.ChairIsDirty);
        }
    }

    @Override protected void Consumption() { }

    private void ChairIsDirty() { setDirty(false); }
    private void SitDown() { Alert.GenerateAlert(this.clock.getMinute(), this.name, "Chair is used..."); }

    //region getters and setters
    protected void setBroken(boolean broken) { this.broken = broken; }

    public boolean isDirty() { return this.dirty; }
    private void setDirty(boolean dirty) { this.dirty = dirty; }
    //endregion
}
