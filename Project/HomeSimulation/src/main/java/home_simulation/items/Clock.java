package home_simulation.items;

import home_simulation.api.IBrokenable;
import home_simulation.items.abstrct.Item;
import home_simulation.api.IElectricityConsumer;
import home_simulation.enums.ActionType;
import home_simulation.enums.ConsumableType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Clock extends Item implements IBrokenable, IElectricityConsumer
{
    private boolean broken = false;
    private boolean started = false;
    private boolean ringing = false;
    private int timer = 0;

    public Clock(String name, Room room, int floor)
    {
        super(name, room, floor);

        this.consumables.put(ConsumableType.Electricity, 0.0);
    }

    @Override
    public void Broke()
    {
        setBroken(true);
        Publish(EventType.Broken);
    }

    @Override
    public void Repair()
    {
        setDegradation(0);
        setBroken(false);
    }

    @Override public boolean IsBroken() { return this.broken; }

    @Override public void TurnOff() { setRinging(false); }
    @Override public void TurnOn() { }
    @Override public boolean IsOn() { return true; }

    @Override public double GetElectricity() { return this.consumables.get(ConsumableType.Electricity); }
    @Override public void SetElectricity(double value) { this.consumables.put(ConsumableType.Electricity, value); }
    @Override public void AddElectricity(Double value) { SetElectricity(GetElectricity() + Math.abs(value)); }

    @Override public boolean IsSporty() { return false; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        var eventTypes = new ArrayList<EventType>();
        eventTypes.add(EventType.ClockAlarm);

        return eventTypes;
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();

        if(IsBroken())
        {
            actions.add(ActionType.Repair);
            return actions;
        }

        if(!this.started)
        {
            actions.add(ActionType.ClockClick);
        }

        if (isRinging())
        {
            actions.add(ActionType.TurnOff);
        }

        return actions;
    }

    @Override
    public void ExecuteAction(ActionType actionType)
    {
        switch (actionType)
        {
            case ClockClick: Click();break;
            case TurnOff:TurnOff();break;
            case Repair:Repair();break;
            default:break;
        }
    }

    @Override
    protected void CustomUpdate()
    {
        var rand = ThreadLocalRandom.current().nextInt(0, 100) % 4 == 0;
        if(this.degradation >= 50 && rand && !IsBroken())
        {
            Broke();
        }

        if(this.started)
        {
            this.timer +=1;
        }

        if (this.timer == 5)
        {
            RingAlarm();
        }
    }

    @Override
    protected void Consumption()
    {
        double consumption = 2;
        if(isOccupied())
        {
            consumption += 1;
        }
        if(IsBroken())
        {
            consumption += 1;
        }
        AddElectricity(consumption);
    }

    private void Click() { this.started = true; }

    private void RingAlarm()
    {
        setRinging(true);
        Publish(EventType.ClockAlarm);
    }

    //region getters and setters
    protected void setBroken(boolean broken) { this.broken = broken; }
    public boolean isRinging() { return this.ringing; }
    private void setRinging(boolean ringing) { this.ringing = ringing; }
    //endregion
}
