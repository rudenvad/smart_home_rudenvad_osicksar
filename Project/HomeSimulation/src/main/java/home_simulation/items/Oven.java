package home_simulation.items;

import home_simulation.Utils.Utils;
import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.api.*;
import home_simulation.enums.ActionType;
import home_simulation.enums.ConsumableType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Oven extends Item implements IBrokenable, IInsertable, IOpenable, IElectricityConsumer, IGasConsumer
{
    private boolean broken = false;
    private boolean on = true;
    private boolean open = false;
    private boolean working = false;
    private int workingTime = 0;

    private List<Utils.Food> content = new ArrayList<Utils.Food>();

    public Oven(String name, Room room, int floor)
    {
        super(name, room, floor);

        this.consumables.put(ConsumableType.Electricity, 0.0);
        this.consumables.put(ConsumableType.Gas, 0.0);
    }

    @Override
    public void Broke()
    {
        setBroken(true);
        Publish(EventType.Broken);
    }

    @Override
    public void Repair()
    {
        setDegradation(0);
        setBroken(false);
    }
    @Override public boolean IsBroken() { return this.broken; }

    @Override
    public void Insert()
    {
        var food = new ArrayList<Utils.Food>();
        var count  = ThreadLocalRandom.current().nextInt(1, 5);
        for(int i = 0; i < count; i++)
        {
            food.add(Utils.Food.GenerateUnique());
        }
        content.addAll(food);
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Food inserted.");
    }

    @Override
    public void TakeOut()
    {
        this.content = new ArrayList<Utils.Food>();
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Food is taken out.");
    }

    @Override public void Open() { this.open = true; }
    @Override public void Close() { this.open = false; }
    @Override public boolean IsOpen() { return this.open; }

    @Override public void TurnOff() { this.on = false; }
    @Override public void TurnOn() { this.on = true; }
    @Override public boolean IsOn() { return this.on; }

    @Override public double GetElectricity() { return this.consumables.get(ConsumableType.Electricity); }
    @Override public void SetElectricity(double value) { this.consumables.put(ConsumableType.Electricity, value); }
    @Override public void AddElectricity(Double value) { SetElectricity(GetElectricity() + Math.abs(value)); }

    @Override public double GetGas() { return this.consumables.get(ConsumableType.Gas); }
    @Override public void SetGas(double value) { this.consumables.put(ConsumableType.Gas, value); }
    @Override public void AddGas(Double value) { SetGas(GetGas() + Math.abs(value)); }

    @Override public boolean IsSporty() { return false; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        var eventTypes = new ArrayList<EventType>();

        eventTypes.add(EventType.OvenWorkingFinished);

        return eventTypes;
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();

        if(IsBroken())
        {
            actions.add(ActionType.Repair);
            return actions;
        }

        if(isWorking())
        {
            actions.add(ActionType.OvenStareAt);
            return actions;
        }

        if(!IsOpen())
        {
            actions.add(ActionType.Open);
            actions.add(ActionType.OvenStart);
            return actions;
        }

        actions.add(ActionType.OvenInsertFood);

        if(this.content.size() > 0)
        {
            actions.add(ActionType.OvenTakeOutFood);
        }

        actions.add(ActionType.Close);

        return actions;
    }

    @Override
    public void ExecuteAction(ActionType actionType)
    {
        switch (actionType)
        {
            case TurnOn: TurnOn();break;
            case TurnOff: TurnOff();break;

            case Open: Open();break;
            case Close: Close();break;

            case OvenInsertFood: Insert();break;
            case OvenTakeOutFood: TakeOut();break;
            case OvenStareAt: Stare();break;
            case OvenStart: StartOven();break;

            case Repair: Repair();break;
            default: break;
        }
    }

    @Override
    protected void CustomUpdate()
    {
        var rand = ThreadLocalRandom.current().nextInt(0, 100) % 4 == 0;
        if(getDegradation() >= 50 && rand && !IsBroken())
        {
            Broke();
        }

        if(isWorking())
        {
            this.workingTime += 1;
        }

        if(this.workingTime == 5)
        {
            this.working = false;
            this.workingTime = 0;
            Publish(EventType.OvenWorkingFinished);
        }
    }

    @Override
    protected void Consumption()
    {
        double electricity = 10;

        if(isWorking())
        {
            electricity = ThreadLocalRandom.current().nextDouble(500, 700);
            var gas = ThreadLocalRandom.current().nextDouble(590, 610);
            AddGas(gas);
        }
        AddElectricity(electricity);
    }

    private void StartOven() { this.working = true; }

    private void Stare() { Alert.GenerateAlert(this.clock.getMinute(), this.name, "Making noise..."); }

    //region getters and setters
    protected void setBroken(boolean broken) { this.broken = broken; }
    public boolean isWorking() { return working; }
    public List<Utils.Food> getContent() { return this.content; }
    //endregion
}
