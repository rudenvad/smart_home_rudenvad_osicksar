package home_simulation.items;

import home_simulation.Utils.Utils;
import home_simulation.entities.Alert;
import home_simulation.api.IBrokenable;
import home_simulation.items.abstrct.Item;
import home_simulation.api.IElectricityConsumer;
import home_simulation.api.IInsertable;
import home_simulation.api.IOpenable;
import home_simulation.enums.ActionType;
import home_simulation.enums.ConsumableType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Fridge extends Item implements IBrokenable, IInsertable, IOpenable, IElectricityConsumer
{
    private boolean broken = false;
    private boolean on = true;
    private boolean open = false;
    private int closeTime = 0;

    private List<Utils.Food> content = new ArrayList<Utils.Food>();

    public Fridge(String name, Room room, int floor)
    {
        super(name, room, floor);

        var itemsCount = ThreadLocalRandom.current().nextInt(5, 15);

        for(int i = 0; i < itemsCount; i++)
        {
            this.content.add(Utils.Food.GenerateUnique());
        }

        this.consumables.put(ConsumableType.Electricity, 0.0);
    }

    @Override
    public void Broke()
    {
        setBroken(true);
        Publish(EventType.Broken);
    }

    @Override
    public void Repair()
    {
        setDegradation(0);
        setBroken(false);
    }
    @Override public boolean IsBroken() { return this.broken; }

    @Override
    public void Insert()
    {
        var food = Utils.Food.GenerateUnique();
        this.content.add(food);
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "New" + food.getName() + "  added.");
    }

    @Override
    public void TakeOut()
    {
        var randFood = this.content.size() == 0? 0 : ThreadLocalRandom.current().nextInt(0, this.content.size());
        var food =this.content.remove(randFood);
        Alert.GenerateAlert(this.clock.getMinute(), this.name, food.getName() + "  taken out.");
    }

    @Override public void Open() { this.open = true; }
    @Override public void Close() { this.open = false; }
    @Override public boolean IsOpen() { return this.open; }

    @Override public void TurnOff() { this.on = false; }
    @Override public void TurnOn() { this.on = true; }
    @Override public boolean IsOn() { return this.on; }

    @Override public double GetElectricity() { return this.consumables.get(ConsumableType.Electricity); }
    @Override public void SetElectricity(double value) { this.consumables.put(ConsumableType.Electricity, value); }
    @Override public void AddElectricity(Double value) { SetElectricity(GetElectricity() + Math.abs(value)); }

    @Override public boolean IsSporty() { return false; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        var eventTypes = new ArrayList<EventType>();

        eventTypes.add(EventType.FridgeCloseAlarm);
        eventTypes.add(EventType.FridgeFoodExpired);
        eventTypes.add(EventType.FridgeIsEmpty);

        return eventTypes;
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();

        if(IsBroken())
        {
            actions.add(ActionType.Repair);
            return actions;
        }

        if (!IsOn())
        {
            actions.add(ActionType.TurnOn);
            return actions;
        }

        if(!IsOpen())
        {
            actions.add(ActionType.Open);
            return actions;
        }

        actions.add(ActionType.Close);

        actions.add(ActionType.FridgeInsertFood);

        if(this.content.size() > 0)
        {
            actions.add(ActionType.FridgeTakeOutFood);
        }

        var expiredFood = this.content.stream().filter(f-> f.getExpiration() <= 0).collect(Collectors.toList());
        if(expiredFood.size() > 0)
        {
            actions.add(ActionType.FridgeTakeOutExpiredFood);
        }

        actions.add(ActionType.FridgeStareAt);

        return actions;
    }

    @Override
    public void ExecuteAction(ActionType actionType)
    {
        switch (actionType)
        {
            case TurnOn: TurnOn();break;
            case TurnOff: TurnOff();break;
            case Open: Open();break;
            case Close: Close();break;
            case FridgeInsertFood: Insert();break;
            case FridgeTakeOutFood: TakeOut(); break;
            case FridgeStareAt: Stare(); break;
            case FridgeTakeOutExpiredFood: FridgeTakeOutExpiredFood(); break;
            case Repair: Repair();break;
            default: break;
        }
    }

    @Override
    protected void CustomUpdate()
    {
        var rand = ThreadLocalRandom.current().nextInt(0, 100) % 4 == 0;
        if(getDegradation() >= 50 && rand && !IsBroken())
        {
            Broke();
        }

        if(!isOccupied() && IsOn() && IsOpen())
        {
            this.closeTime +=1;
        }
        else
        {
            this.closeTime = 0;
        }

        if(this.closeTime > 10 && this.clock.getMinute() % 5 == 0)
        {
            Publish(EventType.FridgeCloseAlarm);
        }

        //Expiration food throw event
        for(var food : this.content)
        {
            if(this.clock.getMinute() % 10 == 0 && food.getExpiration() == 0)
            {
                Publish(EventType.FridgeFoodExpired);
                break;
            }

            food.DecreaseExpiration();
        }

        if(this.clock.getMinute() % 5 == 0 && this.content.size() == 0)
        {
            Publish(EventType.FridgeIsEmpty);
        }
    }

    @Override
    protected void Consumption()
    {
        var consumption = ThreadLocalRandom.current().nextDouble(100, 250);
        if(IsBroken())
        {
            consumption += 20;
        }
        if(IsOpen())
        {
            consumption += 2.5;
        }
        AddElectricity(consumption);
    }

    private void FridgeTakeOutExpiredFood()
    {
        var expiredFood = this.content.stream().filter(f-> f.getExpiration() <= 0).collect(Collectors.toList());

        for(var food : expiredFood) { this.content.remove(food); }

        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Expired food is taken out.");
    }

    private void Stare() { Alert.GenerateAlert(this.clock.getMinute(), this.name,"Buzz-Buzz-Buzz"); }

    //region getters and setters
    protected void setBroken(boolean broken) { this.broken = broken; }
    public List<Utils.Food> getContent() { return content; }
    //endregion
}
