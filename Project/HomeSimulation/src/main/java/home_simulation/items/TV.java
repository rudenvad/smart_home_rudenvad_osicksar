package home_simulation.items;

import home_simulation.entities.Alert;
import home_simulation.api.IBrokenable;
import home_simulation.items.abstrct.Item;
import home_simulation.api.IElectricityConsumer;
import home_simulation.enums.ActionType;
import home_simulation.enums.ConsumableType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static home_simulation.enums.EventType.TVCartoonAlarm;

public class TV extends Item implements IBrokenable, IElectricityConsumer
{
    private boolean broken = false;
    private boolean on;
    private int volume = 15;

    public TV(String name, Room room, int floor)
    {
        super(name, room, floor);

        this.consumables.put(ConsumableType.Electricity, 0.0);
    }

    @Override
    public void Broke()
    {
        setBroken(true);
        Publish(EventType.Broken);
    }

    @Override
    public void Repair()
    {
        setDegradation(0);
        setBroken(false);
    }

    @Override public boolean IsBroken() { return this.broken; }

    @Override public void TurnOff() { this.on = false; }
    @Override public void TurnOn() { this.on = true; }
    @Override public boolean IsOn() { return this.on; }

    @Override public double GetElectricity() { return this.consumables.get(ConsumableType.Electricity); }
    @Override public void SetElectricity(double value) { this.consumables.put(ConsumableType.Electricity, value); }
    @Override public void AddElectricity(Double value) { SetElectricity(GetElectricity() + Math.abs(value)); }

    @Override public boolean IsSporty() { return false; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        var eventTypes = new ArrayList<EventType>();

        eventTypes.add(TVCartoonAlarm);

        return eventTypes;
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();

        if (IsBroken())
        {
            actions.add(ActionType.Repair);
            return actions;
        }

        if (!IsOn())
        {
            actions.add(ActionType.TurnOn);
            return actions;
        }

        actions.add(ActionType.TVVolumeUp);
        actions.add(ActionType.TVVolumeDown);
        actions.add(ActionType.TVChangeChannel);

        return actions;
    }

    @Override
    public void ExecuteAction(ActionType actionType)
    {
        switch (actionType)
        {
            case TurnOn: TurnOn();break;
            case TurnOff: TurnOff();break;
            case TVVolumeUp: VolumeUp();break;
            case TVVolumeDown: VolumeDown();break;
            case TVChangeChannel: ChangeChannel();break;
            case Repair: Repair();break;
            default: break;
        }
    }

    @Override
    protected void CustomUpdate()
    {
        var rand = ThreadLocalRandom.current().nextInt(0, 100) % 4 == 0;
        if (getDegradation() >= 50 && rand && !IsBroken())
        {
            Broke();
        }

        var cartoonRandom = ThreadLocalRandom.current().nextInt(1, 100);
        if(this.clock.getMinute()!= 0 && this.clock.getMinute() % cartoonRandom == 0)
        {
            Publish(TVCartoonAlarm);
        }
    }

    @Override
    protected void Consumption()
    {
        double consumption = 10;
        if (IsOn())
        {
            consumption += 140;
        }
        if (IsBroken())
        {
            consumption += 35;
        }
        AddElectricity(consumption);
    }

    private void VolumeUp()
    {
        if(this.volume == 100)
        {
            Alert.GenerateAlert(this.clock.getMinute(), this.name, "Maximum volume level is already reached.");
            return;
        }
        this.volume+=1;
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Volume changed from " + (this.volume - 1) + " to " + this.volume + ".");
    }

    private void VolumeDown()
    {
        if(this.volume == 0)
        {
            Alert.GenerateAlert(this.clock.getMinute(), this.name, "Minimum volume level is already reached.");
            return;
        }
        this.volume-=1;
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Volume changed from " + (this.volume + 1) + " to " + this.volume + ".");
    }

    private void ChangeChannel() { Alert.GenerateAlert(this.clock.getMinute(), this.name, "Channel changed."); }

    //region getters and setters
    public int getVolume() { return volume; }
    private void setVolume(int volume) { this.volume = volume; }

    protected void setBroken(boolean broken) { this.broken = broken; }
    //endregion
}