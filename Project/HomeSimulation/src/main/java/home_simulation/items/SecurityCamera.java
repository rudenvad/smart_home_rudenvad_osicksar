package home_simulation.items;

import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.api.IBrokenable;
import home_simulation.api.IElectricityConsumer;
import home_simulation.enums.ActionType;
import home_simulation.enums.ConsumableType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class SecurityCamera extends Item implements IBrokenable, IElectricityConsumer
{
    private boolean broken = false;
    private boolean on = false;
    private int memory = 0;

    public SecurityCamera(String name, Room room, int floor)
    {
        super(name, room, floor);

        this.consumables.put(ConsumableType.Electricity, 0.0);
    }

    @Override
    public void Broke()
    {
        setBroken(true);
        Publish(EventType.Broken);
    }

    @Override
    public void Repair()
    {
        setDegradation(0);
        setBroken(false);
    }

    @Override public boolean IsBroken() { return this.broken; }

    @Override public void TurnOff() { this.on = false; }
    @Override public void TurnOn() { this.on = true; }
    @Override public boolean IsOn() { return this.on; }

    @Override public double GetElectricity() { return this.consumables.get(ConsumableType.Electricity); }
    @Override public void SetElectricity(double value) { this.consumables.put(ConsumableType.Electricity, value); }
    @Override public void AddElectricity(Double value) { SetElectricity(GetElectricity() + Math.abs(value)); }

    @Override public boolean IsSporty() { return false; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        var eventTypes = new ArrayList<EventType>();
        eventTypes.add(EventType.StrangeActivityNotification);
        eventTypes.add(EventType.CameraMemoryIsFull);

        return eventTypes;
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();

        if(IsBroken())
        {
            actions.add(ActionType.Repair);
            return actions;
        }

        if(!IsOn())
        {
            actions.add(ActionType.TurnOn);
            return actions;
        }

        actions.add(ActionType.CameraGetRecording);

        actions.add(ActionType.CameraClearMemory);

        actions.add(ActionType.TurnOff);

        return actions;
    }

    @Override
    public void ExecuteAction(ActionType actionType)
    {
        switch (actionType)
        {
            case TurnOn: TurnOn();break;
            case TurnOff: TurnOff();break;

            case CameraGetRecording: GetRecording(); break;
            case CameraClearMemory: ClearMemory(); break;

            case Repair: Repair();break;
            default: break;
        }
    }

    @Override
    protected void CustomUpdate()
    {
        var rand = ThreadLocalRandom.current().nextInt(0, 100) % 4 == 0;
        if(getDegradation() >= 50 && rand && !IsBroken())
        {
            Broke();
        }

        if(IsOn())
        {
            if(this.memory < 60)
            {
                this.memory +=2;
            }

            if(rand && this.clock.getMinute() % 5 == 0)
            {
                Publish(EventType.StrangeActivityNotification);
            }

            if(this.memory >= 60 && this.clock.getMinute() % 7 == 0)
            {
                Publish(EventType.CameraMemoryIsFull);
            }
        }
    }

    @Override
    protected void Consumption()
    {
        if(IsOn())
        {
            double consumption = ThreadLocalRandom.current().nextInt(4, 15);
            AddElectricity(consumption);
        }
    }

    private void GetRecording()
    {
        var min = 10;
        if(this.memory < min)
        {
            Alert.GenerateAlert(this.clock.getMinute(),this.name, "No recordings yet.");
            return;
        }
        var recordingSize = this.memory == min ? min : ThreadLocalRandom.current().nextInt(min, this.memory);

        this.memory -= recordingSize;

        Alert.GenerateAlert(this.clock.getMinute(),this.name, "Recording removed; Current memory: " + this.memory);
    }

    private void ClearMemory()
    {
        this.memory = 0;
        Alert.GenerateAlert(this.clock.getMinute(),this.name, "Memory cleared.");
    }

    //region getters and setters
    protected void setBroken(boolean broken) { this.broken = broken; }
    //endregion
}