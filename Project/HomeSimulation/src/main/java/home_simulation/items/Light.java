package home_simulation.items;

import home_simulation.api.IBrokenable;
import home_simulation.items.abstrct.Item;
import home_simulation.api.IElectricityConsumer;
import home_simulation.enums.ActionType;
import home_simulation.enums.ConsumableType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;

public class Light extends Item implements IBrokenable, IElectricityConsumer
{
    private static final int TIME_OFF_LIMIT = 25;
    private int timeToTurnOff = 0;
    private boolean turnOffTimer = false;
    private boolean on;
    private boolean broken = false;

    public Light(String name, Room room, int floor)
    {
        super(name, room, floor);

        this.consumables.put(ConsumableType.Electricity, 0.0);
    }

    @Override
    public void Broke()
    {
        setBroken(true);
        Publish(EventType.Broken);
    }

    @Override
    public void Repair()
    {
        setDegradation(0);
        setBroken(false);
    }
    @Override public boolean IsBroken() { return this.broken; }

    @Override public void TurnOff() { this.on = false; }
    @Override public void TurnOn() { this.on = true; }
    @Override public boolean IsOn() { return this.on; }

    @Override public double GetElectricity() { return this.consumables.get(ConsumableType.Electricity); }
    @Override public void SetElectricity(double value) { this.consumables.put(ConsumableType.Electricity, value); }
    @Override public void AddElectricity(Double value) { SetElectricity(GetElectricity() + Math.abs(value)); }

    @Override public boolean IsSporty() { return false; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        var eventTypes = new ArrayList<EventType>();
        return eventTypes;
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();

        if(this.IsBroken())
        {
            actions.add(ActionType.Repair);
            return actions;
        }

        if(IsOn())
        {
            actions.add(ActionType.TurnOff);
            actions.add(ActionType.LightSetTurnOffTimer);
        }
        else
        {
            actions.add(ActionType.TurnOn);
        }

        return actions;
    }

    @Override
    public void ExecuteAction(ActionType actionType)
    {
        switch (actionType)
        {
            case TurnOn: TurnOn();break;
            case TurnOff: TurnOff();break;
            case LightSetTurnOffTimer: LightSetTurnOffTimer();break;
            default: break;
        }
    }

    @Override
    protected void CustomUpdate()
    {
        if(isOccupied())
        {
            this.timeToTurnOff = 0;
            this.turnOffTimer = false;
        }

        if(this.timeToTurnOff >= this.TIME_OFF_LIMIT)
        {
            setOn(false);
            this.timeToTurnOff = 0;
            this.turnOffTimer = false;

            Publish(EventType.LightTurnedOff);
        }

        if(IsOn()) { this.timeToTurnOff += 1; }
    }

    @Override
    protected void Consumption()
    {
        double consumption = 6;

        if(this.IsBroken())
        {
            consumption += 4;
        }

        AddElectricity(consumption);
    }

    private void LightSetTurnOffTimer() { this.turnOffTimer = true; }

    //region getters and setters
    protected void setBroken(boolean broken) { this.broken = broken; }
    public void setOn(boolean on) { this.on = on; }
    //endregion
}
