package home_simulation.items.pets;

import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.enums.ActionType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;

public class Cat extends Item
{
    private int hunger = 0;
    private boolean isHungry = false;

    public Cat(String name, Room room, int floor) { super(name, room, floor); }

    @Override public boolean IsSporty() { return false; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        var eventTypes = new ArrayList<EventType>();
        eventTypes.add(EventType.PetIsHungry);
        return eventTypes;
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();
        actions.add(ActionType.PetToStroke);

        if(this.isHungry) { actions.add(ActionType.PetFeed); }

        return actions;
    }

    @Override
    public void ExecuteAction(ActionType actionType)
    {
        switch (actionType)
        {
            case PetToStroke: PetToStroke();break;
            case PetFeed: PetFeed(); break;
            default: break;
        }
    }

    @Override
    protected void CustomUpdate()
    {
        if(this.clock.getMinute() % 3 == 0)
        {
            if(this.hunger < 20)
            {
                this.hunger += 1;
            }
            else
            {
                if(!this.isHungry) { this.isHungry = true; }
                Publish(EventType.PetIsHungry);
            }
        }
    }

    @Override protected void Consumption() { }

    private void PetToStroke() { Alert.GenerateAlert(this.clock.getMinute(),this.name, "Meow-Meow-Meow..."); }

    private void PetFeed()
    {
        this.hunger = 0;
        this.isHungry = false;
        Alert.GenerateAlert(this.clock.getMinute(),this.name, "*Pleased* Meow-Meow-Meow!!!");
    }
}
