package home_simulation.items;

import home_simulation.Utils.Utils;
import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.api.*;
import home_simulation.enums.ActionType;
import home_simulation.enums.ConsumableType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class WashingMachine extends Item implements IBrokenable, IInsertable, IOpenable, IElectricityConsumer, IWaterConsumer
{
    private boolean broken = false;
    private boolean on = true;
    private boolean open = false;
    private boolean washing = false;
    private int washingTime = 0;

    private List<Utils.Clothes> content = new ArrayList<Utils.Clothes>();

    public WashingMachine(String name, Room room, int floor)
    {
        super(name, room, floor);

        var itemsCount = ThreadLocalRandom.current().nextInt(5, 15);

        for(int i = 0; i < itemsCount; i++)
        {
            this.content.add(Utils.Clothes.GenerateUnique());
        }

        this.consumables.put(ConsumableType.Electricity, 0.0);
        this.consumables.put(ConsumableType.Water, 0.0);
    }

    @Override
    public void Broke()
    {
        setBroken(true);
        Publish(EventType.Broken);
    }

    @Override
    public void Repair()
    {
        setDegradation(0);
        setBroken(false);
    }
    @Override public boolean IsBroken() { return this.broken; }

    @Override
    public void Insert()
    {
        var clothes = new ArrayList<Utils.Clothes>();
        var count  = ThreadLocalRandom.current().nextInt(5, 10);
        for(int i = 0; i < count; i++)
        {
            clothes.add(Utils.Clothes.GenerateUnique());
        }
        this.content.addAll(clothes);
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Clothes inserted.");
    }

    @Override
    public void TakeOut()
    {
        this.content = new ArrayList<Utils.Clothes>();
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Clothes is taken out.");
    }

    @Override public void Open() { this.open = true; }
    @Override public void Close() { this.open = false; }
    @Override public boolean IsOpen() { return this.open; }

    @Override public void TurnOff() { this.on = false; }
    @Override public void TurnOn() { this.on = true; }
    @Override public boolean IsOn() { return this.on; }

    @Override public double GetElectricity() { return this.consumables.get(ConsumableType.Electricity); }
    @Override public void SetElectricity(double value) { this.consumables.put(ConsumableType.Electricity, value); }
    @Override public void AddElectricity(Double value) { SetElectricity(GetElectricity() + Math.abs(value)); }

    @Override public double GetWater() { return this.consumables.get(ConsumableType.Water); }
    @Override public void SetWater(double value) { this.consumables.put(ConsumableType.Water, value); }
    @Override public void AddWater(Double value) { SetWater(GetWater() + Math.abs(value)); }

    @Override public boolean IsSporty() { return false; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        var eventTypes = new ArrayList<EventType>();

        eventTypes.add(EventType.WashingFinished);

        return eventTypes;
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();

        if(IsBroken())
        {
            actions.add(ActionType.Repair);
            return actions;
        }

        if(isWashing())
        {
            actions.add(ActionType.WashingMachineStareAt);
            return actions;
        }

        if(!IsOpen())
        {
            actions.add(ActionType.Open);
            actions.add(ActionType.WashingMachineStart);
            return actions;
        }

        actions.add(ActionType.WashingMachineInsertClothes);

        if(this.content.size() > 0)
        {
            actions.add(ActionType.WashingMachineTakeOutClothes);
        }

        actions.add(ActionType.Close);

        return actions;
    }

    @Override
    public void ExecuteAction(ActionType actionType)
    {
        switch (actionType)
        {
            case TurnOn: TurnOn();break;
            case TurnOff: TurnOff();break;
            case Open: Open();break;
            case Close: Close();break;
            case WashingMachineInsertClothes: Insert();break;
            case WashingMachineTakeOutClothes: TakeOut(); break;
            case WashingMachineStareAt: Stare(); break;
            case WashingMachineStart: StartWashing();break;
            case Repair: Repair();break;
            default: break;
        }
    }

    @Override
    protected void CustomUpdate()
    {
        var rand = ThreadLocalRandom.current().nextInt(0, 100) % 4 == 0;
        if(getDegradation() >= 50 && rand && !IsBroken())
        {
            Broke();
        }

        if(isWashing())
        {
            this.washingTime += 1;
        }

        if(this.washingTime == 5)
        {
            this.washing = false;
            this.washingTime = 0;
            Publish(EventType.WashingFinished);
        }
    }

    @Override
    protected void Consumption()
    {
        double electricity = 10;

        if(isWashing())
        {
            electricity = ThreadLocalRandom.current().nextDouble(200, 255);
            var water = ThreadLocalRandom.current().nextDouble(135, 140);
            AddWater(water);
        }
        AddElectricity(electricity);
    }

    private void StartWashing() { this.washing = true; }

    private void Stare()
    {
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Making noise...");
    }

    //region getters and setters
    protected void setBroken(boolean broken) { this.broken = broken; }
    public boolean isWashing() { return washing; }
    public List<Utils.Clothes> getContent() { return this.content; }
    //endregion
}
