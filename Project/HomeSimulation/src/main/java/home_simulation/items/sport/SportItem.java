package home_simulation.items.sport;

import home_simulation.api.IBrokenable;
import home_simulation.items.abstrct.Item;
import home_simulation.enums.ActionType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;

import java.util.ArrayList;
import java.util.List;

public class SportItem extends Item implements IBrokenable
{
    private boolean broken = false;

    public SportItem(String name, Room room, int floor) { super(name, room, floor); }

    @Override
    public void Broke()
    {
        setBroken(true);
        Publish(EventType.Broken);
    }

    @Override
    public void Repair()
    {
        setDegradation(0);
        setBroken(false);
    }

    @Override
    public boolean IsBroken() { return this.broken; }

    @Override public boolean IsSporty() { return true; }

    @Override
    public List<EventType> CustomGetPossibleEventTypes()
    {
        return new ArrayList<EventType>();
    }

    @Override
    public List<ActionType> GetPossibleActions()
    {
        var actions = new ArrayList<ActionType>();

        if(this.IsBroken())
        {
            actions.add(ActionType.Repair);
            return actions;
        }

        return actions;
    }

    @Override public void ExecuteAction(ActionType actionType) { }

    @Override protected void CustomUpdate() { }

    @Override protected void Consumption() { }

    protected void setBroken(boolean broken) { this.broken = broken; }
}
