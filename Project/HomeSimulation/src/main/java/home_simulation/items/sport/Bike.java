package home_simulation.items.sport;

import home_simulation.entities.Alert;
import home_simulation.enums.Room;

public class Bike extends SportItem
{
    public Bike(String name, Room room, int floor) { super(name, room, floor); }

    @Override
    protected void CustomUpdate()
    {
        if(this.isOccupied())
        {
            Alert.GenerateAlert(this.clock.getMinute(),this.name, "Using gps for tracking location...");
        }
    }
}
