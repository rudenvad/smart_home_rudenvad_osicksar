package home_simulation.actors;

import home_simulation.actors.abstrct.Actor;
import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.enums.ActionType;
import home_simulation.enums.EventType;
import home_simulation.items.Chair;
import home_simulation.items.Fridge;
import home_simulation.items.TV;
import home_simulation.items.pets.Cat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class JuniorChild extends Actor
{
    public JuniorChild(String name) { super(name); }

    @Override
    public List<EventType> GetSupportedEventTypes()
    {
        var supportedEventTypes = new ArrayList<EventType>();
        supportedEventTypes.add(EventType.TVCartoonAlarm);
        supportedEventTypes.add(EventType.FridgeCloseAlarm);
        supportedEventTypes.add(EventType.PetIsHungry);
        supportedEventTypes.add(EventType.ChairIsDirty);

        return supportedEventTypes;
    }

    @Override
    protected void HandleEventCustom(Item item, EventType eventType)
    {
        switch (eventType)
        {
            case TVCartoonAlarm: OnTVCartoonAlarm((TV)item);break;
            case FridgeCloseAlarm: OnFridgeCloseAlarm((Fridge)item); break;
            case PetIsHungry: OnPetIsHungry((Cat)item);break;
            case ChairIsDirty: OnChairIsDirty((Chair)item);break;
            default: break;
        }
    }

    public void OnChairIsDirty(Chair chair)
    {
        MakeAction(chair, ActionType.ChairClear);
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Cleaning chair");
    }

    public void OnPetIsHungry(Cat cat) { MakeAction(cat,ActionType.PetFeed); }

    public void OnFridgeCloseAlarm(Fridge fridge) { MakeAction(fridge,ActionType.Close); }

    public void OnTVCartoonAlarm(TV tv)
    {
        if(tv.isOccupied())
        {
            Alert.GenerateAlert(this.clock.getMinute(), this.name, "Begging to turn channel with cartoon...");
        }
        else
        {
            WatchCartoon(tv);
        }
    }

    private void WatchCartoon(TV tv)
    {
        if(!tv.IsOn())
        {
            MakeAction(tv, ActionType.TurnOn);

            Alert.GenerateAlert(this.clock.getMinute(), this.name, "Watching cartoon.");

            var shouldTurnOff = ThreadLocalRandom.current().nextInt(0, 100) % 4 == 0;

            if(shouldTurnOff)
            {
                MakeAction(tv, ActionType.TurnOff);
            }
        }
    }
}
