package home_simulation.actors;

import home_simulation.actors.abstrct.Actor;
import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.enums.ActionType;
import home_simulation.enums.EventType;
import home_simulation.items.Fridge;
import home_simulation.items.SecurityCamera;

import java.util.ArrayList;
import java.util.List;

public class MiddleChild extends Actor
{
    public MiddleChild(String name) { super(name); }

    @Override
    public List<EventType> GetSupportedEventTypes()
    {
        var supportedEventTypes = new ArrayList<EventType>();
        supportedEventTypes.add(EventType.Broken);
        supportedEventTypes.add(EventType.FridgeCloseAlarm);
        supportedEventTypes.add(EventType.StrangeActivityNotification);
        supportedEventTypes.add(EventType.CameraMemoryIsFull);

        return supportedEventTypes;
    }

    @Override
    protected void HandleEventCustom(Item item, EventType eventType)
    {
        switch (eventType)
        {
            case Broken: OnBroken(item);break;
            case FridgeCloseAlarm: OnFridgeCloseAlarm((Fridge) item);break;
            case CameraMemoryIsFull: OnCameraMemoryIsFull((SecurityCamera)item); break;
            case StrangeActivityNotification: OnStrangeActivityNotification((SecurityCamera)item);break;
            default: break;
        }
    }

    public void OnFridgeCloseAlarm(Fridge fridge) { MakeAction(fridge,ActionType.Close); }

    public void OnStrangeActivityNotification(SecurityCamera camera)
    {
        MakeAction(camera,ActionType.CameraGetRecording);
        Alert.GenerateAlert(clock.getMinute(),this.name, "Checking camera recording.");
    }

    public void OnCameraMemoryIsFull(SecurityCamera camera)
    {
        MakeAction(camera,ActionType.CameraClearMemory);
        Alert.GenerateAlert(clock.getMinute(),this.name, "This camera has always low memory!");
    }

    public void OnBroken(Item item) { Alert.GenerateAlert(clock.getMinute(),this.name, "Not again! Everyone would think it's me broke " + item.getName()); }
}
