package home_simulation.actors.abstrct;

import home_simulation.entities.Action;
import home_simulation.items.abstrct.Item;
import home_simulation.simulation.SimulationClock;
import home_simulation.Utils.Utils;
import home_simulation.enums.ActionType;
import home_simulation.enums.ActorState;
import home_simulation.enums.EventType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * Abstract representation of human being.
 * Actor class makes actions on items
 */
public abstract class Actor
{
    protected String name;
    protected List<Item> knownItems;
    protected Item occupiedItem;
    protected int sportMinutesDiff;
    protected ActorState state;
    protected List<Action> actions;
    protected SimulationClock clock;

    protected abstract void HandleEventCustom(Item item, EventType eventType);
    public abstract List<EventType> GetSupportedEventTypes();

    public Actor(String name)
    {
        if(Utils.IsNullOrWhiteSpace(name))
        {
            throw new IllegalArgumentException("Constructor parameters can't be null, empty or blank!");
        }

        setName(name);
        setKnownItems(new ArrayList<Item>());
        setActions(new ArrayList<Action>());
        setState(ActorState.Waiting);
    }

    public void Register(List<Item> items) { this.knownItems.addAll(items); }

    public void HandleEvent(Item item, EventType eventType) { HandleEventCustom(item, eventType); }

    public void Update()
    {
        if (this.state != ActorState.Waiting)
        {
            if (this.occupiedItem.IsSporty())
            {
                IncreaseSportSecondsDiff();
            }
            else
            {
                DecreaseSportSecondsDiff();

                if (ThreadLocalRandom.current().nextInt(0, 100) % 2 == 0)
                {
                    var actions = this.occupiedItem.GetPossibleActions();

                    if (actions.size() > 0)
                    {
                        var rand = ThreadLocalRandom.current().nextInt(0, actions.size());
                        MakeAction(this.occupiedItem, actions.get(rand));
                    }
                }
            }

            var rand = ThreadLocalRandom.current().nextInt(0,100);
            if (rand % 4 == 0)
            {
                this.actions.add(new Action
                        (
                            this.clock.getMinute(),
                            ActionType.Unoccupy,
                            this.name,
                            this.occupiedItem.getName()
                        ));

                this.occupiedItem.setOccupied(false);
                this.occupiedItem = null;

                this.state = ActorState.Waiting;
            }
        }

        if (this.state == ActorState.Waiting)
        {
            var shouldSportBeSelected = this.sportMinutesDiff < 0;

            var items = this.knownItems.stream()
                    .filter(x -> !x.isOccupied() && x.IsSporty() == shouldSportBeSelected)
                    .collect(Collectors.toList());

            if (items.size() > 0)
            {
                var rand = ThreadLocalRandom.current().nextInt(0, items.size());
                this.occupiedItem = items.get(rand);

                this.occupiedItem.setOccupied(true);
                this.state = shouldSportBeSelected ? ActorState.UsingSportItem : ActorState.UsingItem;

                this.actions.add(new Action
                (
                    this.clock.getMinute(),
                    ActionType.Occupy,
                    this.name,
                    this.occupiedItem.getName()
                ));
            }
        }
    }

    protected void MakeAction(Item item, ActionType actionType)
    {
        if (item.GetPossibleActions().contains(actionType))
        {
            this.actions.add(new Action(this.clock.getMinute(), actionType, this.name, item.getName()));

            item.ExecuteAction(actionType);
        }
    }

    protected void IncreaseSportSecondsDiff() { this.sportMinutesDiff++; }
    protected void DecreaseSportSecondsDiff() { this.sportMinutesDiff--; }

    //region getters and setters
    public String getName() { return name; }
    protected void setName(String name) { this.name = name; }

    public List<Item> getKnownItems() { return knownItems; }
    public void setKnownItems(List<Item> knownItems) { this.knownItems = knownItems; }

    public Item getOccupiedItem() { return occupiedItem; }

    public int getSportMinutesDiff() { return sportMinutesDiff; }

    public ActorState getState() { return state; }
    protected void setState(ActorState state) { this.state = state; }

    public List<Action> getActions() { return actions; }
    protected void setActions(List<Action> actions) { this.actions = actions; }

    public SimulationClock getClock() { return clock; }
    public void setClock(SimulationClock clock) { this.clock = clock; }
    //endregion
}
