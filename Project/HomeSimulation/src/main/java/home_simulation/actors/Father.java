package home_simulation.actors;

import home_simulation.Utils.Utils;
import home_simulation.actors.abstrct.Actor;
import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.enums.ActionType;
import home_simulation.enums.EventType;
import home_simulation.items.Fridge;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Father extends Actor
{
    public Father(String name) { super(name); }

    @Override
    public List<EventType> GetSupportedEventTypes()
    {
        var supportedEventTypes = new ArrayList<EventType>();
        supportedEventTypes.add(EventType.Broken);
        supportedEventTypes.add(EventType.ClockAlarm);
        supportedEventTypes.add(EventType.FridgeIsEmpty);
        supportedEventTypes.add(EventType.WashingFinished);
        supportedEventTypes.add(EventType.ReceivedEmailNotification);

        return supportedEventTypes;
    }

    @Override
    protected void HandleEventCustom(Item item, EventType eventType)
    {
        switch (eventType)
        {
            case ClockAlarm: OnClockAlarm(item);break;
            case Broken: OnBroken(item);break;
            case FridgeIsEmpty: OnFridgeIsEmpty((Fridge)item);break;
            case WashingFinished: OnWashingFinished();break;
            case ReceivedEmailNotification: OnReceivedEmailNotification();break;
            default: break;
        }
    }

    public void OnReceivedEmailNotification() { Alert.GenerateAlert(this.clock.getMinute(), this.name, "Read email and reply..."); }

    public void OnWashingFinished() { Alert.GenerateAlert(this.clock.getMinute(),this.name, "Washing is finished, i'm out of here..."); }

    public void OnFridgeIsEmpty(Fridge fridge)
    {
        var products = BuyProducts();

        for(var product : products)
        {
            fridge.getContent().add(product);
        }

        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Filled fridge with bought food.");
    }

    public void OnBroken(Item item) { MakeAction(item, ActionType.Repair); }

    public void OnClockAlarm(Item item)
    {
        var ignore = ThreadLocalRandom.current().nextDouble(0, 100) % 5 == 0;
        if(ignore)
        {
            Alert.GenerateAlert(this.clock.getMinute(),this.name,"Ignoring clock alarm...");
        }
        else
        {
            MakeAction(item, ActionType.TurnOff);
        }
    }

    private List<Utils.Food> BuyProducts()
    {
        var products = new ArrayList<Utils.Food>();
        products.add(Utils.Food.GenerateUnique());
        var productsCount = ThreadLocalRandom.current().nextInt(10, 15);
        for(int i = 0; i < productsCount; i++)
        {
            products.add(Utils.Food.GenerateUnique());
        }
        return products;
    }
}
