package home_simulation.actors;

import home_simulation.actors.abstrct.Actor;
import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.enums.ActionType;
import home_simulation.enums.EventType;
import home_simulation.items.Fridge;
import home_simulation.items.Oven;
import home_simulation.items.WashingMachine;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GrandMother extends Actor
{
    public GrandMother(String name) { super(name); }

    @Override
    public List<EventType> GetSupportedEventTypes()
    {
        var supportedEventTypes = new ArrayList<EventType>();
        supportedEventTypes.add(EventType.FridgeCloseAlarm);
        supportedEventTypes.add(EventType.FridgeFoodExpired);
        supportedEventTypes.add(EventType.FridgeIsEmpty);
        supportedEventTypes.add(EventType.WashingFinished);
        supportedEventTypes.add(EventType.OvenWorkingFinished);

        return supportedEventTypes;
    }

    @Override
    protected void HandleEventCustom(Item item, EventType eventType)
    {
        switch (eventType)
        {
            case FridgeCloseAlarm: OnFridgeCloseAlarm((Fridge)item);break;
            case FridgeFoodExpired: OnFridgeFoodExpired((Fridge)item);break;
            case FridgeIsEmpty: OnFridgeIsEmpty((Fridge)item);break;
            case WashingFinished: OnWashingMachineFinished((WashingMachine) item); break;
            case OvenWorkingFinished: OnOvenWorkingFinished((Oven)item);break;
            default: break;
        }
    }

    public void OnOvenWorkingFinished(Oven oven)
    {
        MakeAction(oven, ActionType.Open);
        MakeAction(oven,ActionType.OvenTakeOutFood);
        MakeAction(oven, ActionType.Close);
        Alert.GenerateAlert(this.clock.getMinute(),this.name, "Dish is ready.");
    }

    public void OnWashingMachineFinished(WashingMachine washingMachine)
    {
        MakeAction(washingMachine, ActionType.Open);
        MakeAction(washingMachine,ActionType.WashingMachineTakeOutClothes);
        Alert.GenerateAlert(this.clock.getMinute(),this.name, "Hang the clothes.");
    }

    public void OnFridgeIsEmpty(Fridge fridge) { Alert.GenerateAlert(this.clock.getMinute(), this.name, "*Grumble* Fridge is empty again!"); }

    public void OnFridgeFoodExpired(Fridge fridge)
    {
        var expiredFood= fridge.getContent().stream().filter(f-> f.getExpiration() <=0).collect(Collectors.toList());

        for(var food : expiredFood)
        {
            MakeAction(fridge, ActionType.FridgeTakeOutExpiredFood);
        }
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "What a waste of money!");
    }

    public void OnFridgeCloseAlarm(Fridge fridge)
    {
        MakeAction(fridge,ActionType.Close);
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "*Grumble* Fridge is not closed again?!.");
    }
}
