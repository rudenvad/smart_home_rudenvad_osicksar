package home_simulation.actors;

import home_simulation.actors.abstrct.Actor;
import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.enums.ActionType;
import home_simulation.enums.EventType;
import home_simulation.items.Clock;
import home_simulation.items.Light;
import home_simulation.items.pets.Cat;

import java.util.ArrayList;
import java.util.List;

public class GrandFather extends Actor
{
    public GrandFather(String name) { super(name); }

    @Override
    public List<EventType> GetSupportedEventTypes()
    {
        var supportedEventTypes = new ArrayList<EventType>();
        supportedEventTypes.add(EventType.Broken);
        supportedEventTypes.add(EventType.ClockAlarm);
        supportedEventTypes.add(EventType.LightTurnedOff);
        supportedEventTypes.add(EventType.PetIsHungry);

        return supportedEventTypes;
    }

    @Override
    protected void HandleEventCustom(Item item, EventType eventType)
    {
        switch (eventType)
        {
            case Broken: OnBroken(item);break;
            case ClockAlarm: OnClockAlarm((Clock)item);break;
            case LightTurnedOff: OnLightTurnedOff((Light)item);break;
            case PetIsHungry: OnPetIsHungry((Cat)item);break;
            default: break;
        }
    }

    public void OnPetIsHungry(Cat cat) { MakeAction(cat,ActionType.PetFeed); }

    public void OnLightTurnedOff(Light light) { MakeAction(light, ActionType.LightSetTurnOffTimer); }

    public void OnBroken(Item item) { MakeAction(item, ActionType.Repair); }

    public void OnClockAlarm(Clock clock)
    {
        MakeAction(clock, ActionType.ClockClick);
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Setting alarm...");
    }
}