package home_simulation.actors;

import home_simulation.Utils.Utils;
import home_simulation.actors.abstrct.Actor;
import home_simulation.entities.Alert;
import home_simulation.items.abstrct.Item;
import home_simulation.enums.ActionType;
import home_simulation.enums.EventType;
import home_simulation.items.Fridge;
import home_simulation.items.WashingMachine;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Mother extends Actor
{
    public Mother(String name) { super(name); }

    @Override
    public List<EventType> GetSupportedEventTypes()
    {
        var supportedEventTypes = new ArrayList<EventType>();
        supportedEventTypes.add(EventType.Broken);
        supportedEventTypes.add(EventType.ClockAlarm);
        supportedEventTypes.add(EventType.FridgeCloseAlarm);
        supportedEventTypes.add(EventType.FridgeFoodExpired);
        supportedEventTypes.add(EventType.FridgeIsEmpty);
        supportedEventTypes.add(EventType.WashingFinished);

        return supportedEventTypes;
    }

    @Override
    protected void HandleEventCustom(Item item, EventType eventType)
    {
        switch (eventType)
        {
            case ClockAlarm: OnClockAlarm(item);break;
            case Broken: OnBroken(item);break;
            case FridgeCloseAlarm: OnFridgeCloseAlarm((Fridge)item);break;
            case FridgeFoodExpired: OnFridgeFoodExpired((Fridge)item);break;
            case FridgeIsEmpty: OnFridgeIsEmpty((Fridge)item);break;
            case WashingFinished: OnWashingMachineFinished((WashingMachine) item); break;
            default: break;
        }
    }

    public void OnWashingMachineFinished(WashingMachine washingMachine)
    {
        MakeAction(washingMachine,ActionType.Open);
        MakeAction(washingMachine,ActionType.WashingMachineTakeOutClothes);
        Alert.GenerateAlert(this.clock.getMinute(),this.name, "Laundry, here we go again...");
    }

    public void OnFridgeIsEmpty(Fridge fridge)
    {
        var products = BuyProducts();

        for(var product : products)
        {
            fridge.getContent().add(product);
        }

        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Filled fridge with bought food.");
    }

    public void OnBroken(Item item)
    {
        var ignore = ThreadLocalRandom.current().nextDouble(0, 100) % 5 == 0;
        if(ignore)
        {
            Alert.GenerateAlert(this.clock.getMinute(), this.name, "Tell father to fix it.");
        }
        else
        {
            MakeAction(item, ActionType.Repair);
        }
    }

    public void OnFridgeFoodExpired(Fridge fridge)
    {
        var expiredFood= fridge.getContent().stream().filter(f-> f.getExpiration() <=0).collect(Collectors.toList());

        if(expiredFood.size() > 0 )
        {
            MakeAction(fridge, ActionType.FridgeTakeOutExpiredFood);
        }
    }

    public void OnFridgeCloseAlarm(Fridge fridge)
    {
        MakeAction(fridge,ActionType.Close);
        Alert.GenerateAlert(this.clock.getMinute(), this.name, "Grumble because fridge again not closed.");
    }

    public void OnClockAlarm(Item item) { MakeAction(item, ActionType.TurnOff); }

    private List<Utils.Food> BuyProducts()
    {
        var products = new ArrayList<Utils.Food>();
        products.add(Utils.Food.GenerateUnique());
        var productsCount = ThreadLocalRandom.current().nextInt(10, 15);
        for(int i = 0; i < productsCount; i++)
        {
            products.add(Utils.Food.GenerateUnique());
        }
        return products;
    }
}

