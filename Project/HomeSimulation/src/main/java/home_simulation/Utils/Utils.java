package home_simulation.Utils;
import home_simulation.actors.*;
import home_simulation.actors.abstrct.Actor;
import home_simulation.items.abstrct.Item;
import home_simulation.simulation.Simulation;
import home_simulation.simulation.SimulationFactory;
import home_simulation.enums.Room;
import home_simulation.items.*;
import home_simulation.items.pets.Cat;
import home_simulation.items.sport.Bike;
import home_simulation.items.sport.Ski;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class Utils
{
    public final static double ELECTRICITY_COST = 4.6;
    public final static double GAS_COST = 6.2;
    public final static double WATER_COST = 90;

    public static final String REPORTS_FOLDER_NAME = "reports";
    public static final String TARGET_FOLDER ="target/";
    public static final String REPORTS_PATH = TARGET_FOLDER + REPORTS_FOLDER_NAME + "/";
    public static final String JSON_TYPE = ".json";

    public final static String[] MALE_NAMES =
    {
        "Liam",
        "Noah",
        "Oliver",
        "Elijah",
        "William",
        "James",
        "Benjamin",
        "Lucas",
        "Henry",
        "Alexander",
    };

    public final static String[] FEMALE_NAMES =
    {
            "Olivia",
            "Emma",
            "Ava",
            "Charlotte",
            "Sophia",
            "Amelia",
            "Isabella",
            "Mia",
            "Evelyn",
            "Harper",
    };

    /**
     * @return Simulation configured manually for comparison
     */
    public static Simulation FirstManualSimulation()
    {
        //create simulation name and time
        var simulationName = "First_Simulation";
        var simulationTime = 60;

        //create 6 actors
        var grandFather = new GrandFather("GrandFather");
        var grandMother = new GrandMother("GrandMother");

        var mother = new Mother("Mother");
        var father = new Father("Father");

        var middleChild = new MiddleChild("MiddleChild");
        var juniorChild =  new JuniorChild("JuniorChild");

        //add actors to list
        var actors = new ArrayList<Actor>();
        actors.add(grandFather);
        actors.add(grandMother);
        actors.add(mother);
        actors.add(father);
        actors.add(middleChild);
        actors.add(juniorChild);

        //create items (8 types devices, 3 sport and 3 pets)
        var items = new ArrayList<Item>();

        int floor = -1;
        //region -1 level
        items.add(new WashingMachine(Room.Laundry + "WashingMachine[-1]", Room.Laundry, floor));
        //endregion

        floor = 0;
        //region 0 level

        //pets
        items.add(new Cat("Cat1", Room.Kitchen, floor));
        items.add(new Cat("Cat2", Room.Bedroom, floor));
        items.add(new Cat("Cat3", Room.Livingroom, floor));

        //Sport items: 2 bikes and 1 ski
        items.add(new Bike("Regular bike", Room.Garage, floor));
        items.add(new Bike("Sport bike", Room.Garage, floor));
        items.add(new Ski("Ski", Room.Garage, floor));

        items.add(new SecurityCamera(Room.Garage + "Camera[0]", Room.Garage,floor));
        items.add(new Light(Room.Garage + "Light[0]", Room.Garage, floor));

        items.add(new Clock(Room.Livingroom + "clock[0]", Room.Livingroom,floor));
        items.add(new TV(Room.Bedroom + "TV[0]", Room.Bedroom, floor));
        items.add(new PC(Room.Bedroom + "PC[0]", Room.Bedroom, floor));
        items.add(new Clock(Room.Bedroom + "clock[0]", Room.Bedroom,floor));

        items.add(new Fridge(Room.Kitchen + "Fridge[0]", Room.Kitchen, floor));
        items.add(new Oven(Room.Kitchen+"Oven[0]",Room.Kitchen,floor));

        items.add(new Light(Room.Kitchen + "Light[0]", Room.Kitchen, floor));
        items.add(new Light(Room.Bedroom + "Light[0]", Room.Bedroom, floor));
        items.add(new Light(Room.Livingroom + "Light[0]", Room.Livingroom, floor));
        //endregion

        floor = 1;
        //region 1 level
        items.add(new Clock(Room.Bedroom + "clock[1]", Room.Bedroom,floor));
        items.add(new PC(Room.Bedroom + "PC[1]", Room.Bedroom, floor));
        items.add(new SecurityCamera(Room.Bedroom + "Camera[1]", Room.Bedroom,floor));

        items.add(new PC(Room.Office + "PC[1]", Room.Office, floor));
        items.add(new TV(Room.Office + "TV[1]", Room.Office, floor));

        items.add(new WashingMachine(Room.Bathroom + "WashingMachine[1]", Room.Bathroom, floor));
        //endregion

        floor = 2;
        //region 2 level
        items.add(new TV(Room.Attic + "TV[2]", Room.Attic, floor));
        items.add(new Light(Room.Attic + "Light[2]", Room.Attic, floor));
        items.add(new Chair(Room.Attic + "Chair[2]", Room.Attic, floor));
        //endregion

        return SimulationFactory.Instance().CreateSimulation(simulationName,simulationTime,actors,items);
    }

    /**
     * @return Simulation configured manually for comparison
     */
    public static Simulation SecondManualSimulation()
    {
        //create simulation name and time
        var simulationName = "Second_Simulation";
        var simulationTime = 60;

        //create 6 actors
        var grandFather = new GrandFather("GrandFather");
        var grandMother = new GrandMother("GrandMother");

        var mother = new Mother("Mother");
        var father = new Father("Father");

        var middleChild = new MiddleChild("MiddleChild");
        var juniorChild =  new JuniorChild("JuniorChild");

        //add actors to list
        var actors = new ArrayList<Actor>();
        actors.add(grandFather);
        actors.add(grandMother);
        actors.add(mother);
        actors.add(father);
        actors.add(middleChild);
        actors.add(juniorChild);

        //create items (8 types devices, 3 sport and 3 pets)
        var items = new ArrayList<Item>();

        int floor = -1;
        //region -1 level
        items.add(new SecurityCamera(Room.Laundry + "Camera[-1]", Room.Laundry,floor));
        //endregion

        floor = 0;
        //region 0 level

        //pets
        items.add(new Cat("Cat1", Room.Office, floor));
        items.add(new Cat("Cat2", Room.Kitchen, floor));
        items.add(new Cat("Cat3", Room.Bedroom, floor));

        //Sport items: 2 bikes and 1 ski
        items.add(new Bike("Regular bike", Room.Garage, floor));
        items.add(new Bike("Sport bike", Room.Garage, floor));
        items.add(new Ski("Ski", Room.Garage, floor));

        items.add(new SecurityCamera(Room.Garage + "Camera[0]", Room.Garage,floor));
        items.add(new Light(Room.Garage + "Light[0]", Room.Garage, floor));

        items.add(new Clock(Room.Livingroom + "clock[0]", Room.Livingroom,floor));
        items.add(new TV(Room.Bedroom + "TV[0]", Room.Bedroom, floor));
        items.add(new PC(Room.Bedroom + "PC[0]", Room.Bedroom, floor));
        items.add(new PC(Room.Bedroom + "SecondPC[0]", Room.Bedroom, floor));
        items.add(new Clock(Room.Bedroom + "clock[0]", Room.Bedroom,floor));

        items.add(new Fridge(Room.Kitchen + "Fridge[0]", Room.Kitchen, floor));
        items.add(new Oven(Room.Kitchen+"Oven[0]",Room.Kitchen,floor));
        items.add(new Oven(Room.Kitchen+"SecondOven[0]",Room.Kitchen,floor));

        items.add(new Light(Room.Kitchen + "Light[0]", Room.Kitchen, floor));
        items.add(new Light(Room.Bedroom + "Light[0]", Room.Bedroom, floor));
        items.add(new Light(Room.Livingroom + "Light[0]", Room.Livingroom, floor));
        items.add(new Light(Room.Bathroom + "Light[2]", Room.Bathroom, floor));
        //endregion

        floor = 1;
        //region 1 level
        items.add(new Clock(Room.Bedroom + "clock[1]", Room.Bedroom,floor));
        items.add(new PC(Room.Bedroom + "PC[1]", Room.Bedroom, floor));
        items.add(new SecurityCamera(Room.Bedroom + "Camera[1]", Room.Bedroom,floor));

        items.add(new PC(Room.Office + "PC[1]", Room.Office, floor));
        items.add(new TV(Room.Office + "TV[1]", Room.Office, floor));

        items.add(new WashingMachine(Room.Bathroom + "WashingMachine[1]", Room.Bathroom, floor));
        items.add(new Clock(Room.Bathroom + "clock[1]", Room.Bathroom,floor));

        items.add(new TV(Room.Livingroom + "TV[1]", Room.Livingroom, floor));
        items.add(new Light(Room.Livingroom + "Light[1]", Room.Livingroom, floor));
        //endregion

        floor = 2;
        //region 2 level
        items.add(new TV(Room.Attic + "TV[2]", Room.Attic, floor));
        items.add(new Light(Room.Attic + "Light[2]", Room.Attic, floor));
        items.add(new WashingMachine(Room.Attic + "WashingMachine[2]", Room.Attic, floor));
        //endregion

        return SimulationFactory.Instance().CreateSimulation(simulationName,simulationTime,actors,items);
    }

    public static String GetRandomMaleName() { return MALE_NAMES[ThreadLocalRandom.current().nextInt(0, MALE_NAMES.length)]; }
    public static String GetRandomFemaleName() { return FEMALE_NAMES[ThreadLocalRandom.current().nextInt(0, FEMALE_NAMES.length)]; }

    /**
     * Checks if string null, blank or empty.
     * @param string
     * @return false if string is null or blank or empty
     */
    public static boolean IsNullOrWhiteSpace(String string)
    {
        return string == null || string.isEmpty() || string.isBlank();
    }

    /**
     * Class representation of content for Fridge item
     */
    public static class Food
    {
        private static int id = 0;
        private String name;
        private Integer expiration;

        public Food(String name, Integer expiration)
        {
            this.name = name;
            this.expiration = expiration;
        }

        /**
         * @return Food with unique id and random expiration date.
         */
        public static Food GenerateUnique()
        {
            var expiration = ThreadLocalRandom.current().nextInt(0, 10);
            var food = new Food("Food-" + id,expiration);
            id++;
            return food;
        }

        public void DecreaseExpiration() { this.expiration--; }
        public String getName() { return this.name; }
        public Integer getExpiration() { return this.expiration; }
    }

    /**
     * Class representation of content for WashingMachine item
     */
    public static class Clothes
    {
        private static int id = 0;
        private String name;

        public Clothes(String name) { this.name = name; }

        /**
         * @return Clothes with unique id
         */
        public static Clothes GenerateUnique()
        {
            var clothes = new Clothes("Clothes-" + id);
            id++;
            return clothes;
        }

        public String getName() { return this.name; }
    }
}
