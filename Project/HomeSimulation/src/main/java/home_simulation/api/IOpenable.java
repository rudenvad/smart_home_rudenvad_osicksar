package home_simulation.api;

public interface IOpenable
{
    void Open();
    void Close();
    boolean IsOpen();
}
