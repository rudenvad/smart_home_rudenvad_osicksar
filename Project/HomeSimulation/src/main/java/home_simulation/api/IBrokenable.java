package home_simulation.api;

public interface IBrokenable
{
    void Broke();
    void Repair();
    boolean IsBroken();
}
