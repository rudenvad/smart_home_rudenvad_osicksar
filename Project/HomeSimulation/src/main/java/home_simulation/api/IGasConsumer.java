package home_simulation.api;

public interface IGasConsumer
{
    double GetGas();
    void SetGas(double value);
    void AddGas(Double value);
}
