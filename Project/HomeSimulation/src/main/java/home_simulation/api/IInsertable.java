package home_simulation.api;

public interface IInsertable
{
    void Insert();
    void TakeOut();
}
