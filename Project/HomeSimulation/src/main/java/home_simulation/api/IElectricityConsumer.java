package home_simulation.api;

public interface IElectricityConsumer
{
    void TurnOn();
    void TurnOff();
    boolean IsOn();

    double GetElectricity();
    void SetElectricity(double value);
    void AddElectricity(Double value);
}
