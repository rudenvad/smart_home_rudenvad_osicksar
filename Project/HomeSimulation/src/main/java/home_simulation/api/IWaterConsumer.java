package home_simulation.api;

public interface IWaterConsumer
{
    double GetWater();
    void SetWater(double value);
    void AddWater(Double value);
}
