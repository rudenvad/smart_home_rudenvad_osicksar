package home_simulation.simulation;

import home_simulation.actors.*;
import home_simulation.actors.abstrct.Actor;
import home_simulation.Utils.Utils;
import home_simulation.enums.Room;
import home_simulation.items.*;
import home_simulation.items.abstrct.Item;
import home_simulation.items.pets.Cat;
import home_simulation.items.sport.Bike;
import home_simulation.items.sport.Ski;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class SimulationFactory
{
    //Singleton pattern
    private static SimulationFactory instance = null;
    private static final Object mutex = new Object();

    /**
     * Checks parameters for simulation and creates it.
     * @param simulationName
     * @param simulationTime
     * @param actors
     * @param items
     * @return simulation by given parameters.
     */
    public Simulation CreateSimulation(String simulationName, Integer simulationTime, List<Actor> actors, List<Item> items)
    {
        if(Utils.IsNullOrWhiteSpace(simulationName) || actors == null || items == null) { throw new IllegalArgumentException("Constructor parameters can't be null, empty or blank!"); }

        if(simulationTime <= 0) { throw new IllegalArgumentException("Simulation time should be grated than zero!"); }

        if(actors.size() < 6) { throw new IllegalArgumentException("["+actors.size()+"] "+"Should be minimum 6 actors!");}

        if(items.size() < 26) { throw new IllegalArgumentException("["+items.size()+"] " + "Should be minimum 26 items (20 devices, 3 sport, 3 pets)!");}

        var itemTypes = new ArrayList<String>();

        for(var item : items)
        {
            if(!itemTypes.contains(item.getClass().getName())) { itemTypes.add(item.getClass().getName()); }
        }

        //8 base types, 2 sport types and atleast 1 pet type
        if(itemTypes.size() < 11)
        {
            throw new IllegalArgumentException("Should be minimum 11 different item types!");
        }

        return new Simulation(simulationName, simulationTime, actors, items);
    }

    /**
     * @return simulation created by pseudo random
     */
    public Simulation CreateRandomSimulation()
    {
        //region create unique simulation name
        var simulationId = UUID.randomUUID();
        var simulationName = "Simulation-" + simulationId.toString();
        //endregion

        //region create simulation time
        var simulationTime = ThreadLocalRandom.current().nextInt(60, 121);
        //endregion

        //region create actors
        var grandFather = new GrandFather(Utils.GetRandomMaleName() + "-grf");
        var grandMother = new GrandMother(Utils.GetRandomFemaleName() + "-grm");

        var father = new Father(Utils.GetRandomMaleName() + "-f");
        var mother = new Mother(Utils.GetRandomFemaleName() + "-m");

        var randMaleOrFemaleName = ThreadLocalRandom.current().nextInt(0, 10) % 2 == 0 ? Utils.GetRandomMaleName() : Utils.GetRandomFemaleName();
        var middleChild = new MiddleChild(randMaleOrFemaleName + "-chm");

        randMaleOrFemaleName = ThreadLocalRandom.current().nextInt(0, 10) % 2 == 0 ? Utils.GetRandomMaleName() : Utils.GetRandomFemaleName();
        var juniorChild =  new JuniorChild(randMaleOrFemaleName + "-chjr");
        //endregion

        //region add actors to list
        var actors = new ArrayList<Actor>();
        actors.add(grandFather);
        actors.add(grandMother);
        actors.add(mother);
        actors.add(father);
        actors.add(middleChild);
        actors.add(juniorChild);
        //endregion

        //region create and add items to list
        int index = 1;
        //Basic items
        var clock = new Clock("Clock-" + index, Room.Bedroom,0);
        var tv = new TV(Room.Bedroom + "TV-" + index, Room.Livingroom, 0);
        var washingMachine = new WashingMachine("WashingMachine-" + index, Room.Laundry, 0);
        var fridge = new Fridge("Fridge-" + index, Room.Kitchen, 0);
        var oven = new Oven("Oven-" + index, Room.Kitchen,0);
        var pc = new PC("PC-" + index, Room.Office, 1);
        var securityCamera = new SecurityCamera("Camera-" + index, Room.Office,1);
        var light = new Light(Room.Bedroom + "Light-" + index, Room.Bedroom, 0);

        //pets;
        var firstCat = new Cat("Cat-" + 1, Room.Kitchen, 0);
        var secondCat = new Cat("Cat-" + 2, Room.Bedroom, 0);
        var thirdCat = new Cat("Cat-" + 3, Room.Livingroom, 0);

        //sport items;
        var sportBike = new Bike("Sport Bike", Room.Garage, 0);
        var regularBike = new Bike("Regular Bike", Room.Garage, 0);
        var ski = new Ski("Ski", Room.Garage, 0);

        var items = new ArrayList<Item>();
        //adding 3 pets
        items.add(firstCat);
        items.add(secondCat);
        items.add(thirdCat);
        //adding 3 sport items
        items.add(sportBike);
        items.add(regularBike);
        items.add(ski);
        //adding basic items (8 different types)
        items.add(clock);
        items.add(tv);
        items.add(washingMachine);
        items.add(fridge);
        items.add(oven);
        items.add(pc);
        items.add(securityCamera);
        items.add(light);
        //region adding random items
        var ignore = false;
        Room randType = Room.Livingroom;
        Item randomItem = null;
        for(int i = 0; i < 5; i++)
        {
            index++;

            if(!ignore)
            {
                if(ThreadLocalRandom.current().nextInt(0, 100) % 5 == 0)
                {
                    randomItem = new PC("PC-" + index, Room.Bedroom, ThreadLocalRandom.current().nextInt(0, 2));
                }
                else
                {
                    if(ThreadLocalRandom.current().nextInt(0, 1) % 2 == 0) { randType = Room.Kitchen; }
                    randomItem = new Clock("Clock-" + index, randType, 0);
                }
                ignore = true;

                items.add(randomItem);

                if(ThreadLocalRandom.current().nextInt(0, 100) % 3 == 0)
                {
                    if(ThreadLocalRandom.current().nextInt(0, 1) % 2 == 0) { randType = Room.Garage; }
                    else { randType = Room.Laundry; }
                    randomItem = new SecurityCamera("Camera-" + index, randType, 0);
                }
                else
                {
                    var rand= ThreadLocalRandom.current().nextInt(0, 100);
                    randType = Room.Bedroom;
                    if(rand % 7 == 0 || rand % 3 == 0) { randType = Room.Kitchen; }
                    else if(rand % 5 == 0) { randType = Room.Office; }
                    randomItem = new TV("TV-" + index, randType, 0);
                }
                items.add(randomItem);
            }

            if(ignore)
            {
                var rand = ThreadLocalRandom.current().nextInt(0, 100);
                if(rand % 4 == 0 || rand % 3 == 0)
                {
                    randomItem = new Light("Light-" + index, Room.Attic, 2);
                }
                else if(ThreadLocalRandom.current().nextInt(0, 100) % 2 == 0)
                {
                    if(ThreadLocalRandom.current().nextInt(0, 1) % 2 == 0) { randType = Room.Laundry; }
                    else { randType = Room.Bathroom; }
                    randomItem = new WashingMachine("WashingMachine-" + index, randType, 0);
                }
                else
                {
                    randomItem = new Oven("Oven-" + index, Room.Kitchen, 0);
                }
            }

            items.add(randomItem);

            if(ThreadLocalRandom.current().nextInt(0, 100) % 6 == 0)
            {
                if(ThreadLocalRandom.current().nextInt(0, 1) % 2 == 0) { randType = Room.Bedroom; }
                else { randType = Room.Livingroom; }
                randomItem = new Clock("Clock-" + index, randType, ThreadLocalRandom.current().nextInt(0, 2));
            }
            else
            {
                var rand= ThreadLocalRandom.current().nextInt(0, 100);
                if(rand % 7 == 0 || rand % 3 == 0) { randType = Room.Laundry; }
                else if(rand % 5 == 0) { randType = Room.Bathroom; }
                randomItem = new SecurityCamera("Camera-" + ++index, randType, 0);
            }

            items.add(randomItem);
        }

        if(ThreadLocalRandom.current().nextInt(0, 100) % 4 == 0)
        {
            randomItem = new Clock("CLock-" + ++index, Room.Office,1);
        }
        else
        {
            randomItem = new PC("PC-" + ++index, Room.Livingroom, ThreadLocalRandom.current().nextInt(0, 2));
        }
        items.add(randomItem);
        //endregion
        //endregion

        return CreateSimulation(simulationName, simulationTime, actors, items);
    }

    //Singleton pattern
    public static SimulationFactory Instance()
    {
        if (instance == null)
        {
            synchronized (mutex)
            {
                if (instance == null) { instance = new SimulationFactory(); }
            }
        }
        return instance;
    }
}
