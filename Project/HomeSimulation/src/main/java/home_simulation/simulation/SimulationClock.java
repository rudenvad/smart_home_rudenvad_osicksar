package home_simulation.simulation;

/**
 * Class responsible for keeping simulation time
 */
public class SimulationClock
{
    private int minute;

    public void Tick()
    {
        minute++;
    }

    public int getMinute() { return minute; }
    private void setMinute(int minute) { this.minute = minute; }
}
