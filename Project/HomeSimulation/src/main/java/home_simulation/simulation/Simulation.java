package home_simulation.simulation;

import com.fasterxml.jackson.databind.ObjectMapper;
import home_simulation.actors.abstrct.Actor;
import home_simulation.entities.Alert;
import home_simulation.Utils.Utils;
import home_simulation.enums.ConsumableType;
import home_simulation.enums.EventType;
import home_simulation.enums.Room;
import home_simulation.entities.reports.ConsumptionReport;
import home_simulation.entities.reports.EventReport;
import home_simulation.items.abstrct.Item;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Simulation
{
    private String name;
    private int minutes;
    private SimulationClock clock;
    private List<Actor> actors;
    private List<Item> items;

    public Simulation(String name, int minutes, List<Actor> actors, List<Item> items)
    {
        setName(name);
        setActors(actors);
        setItems(items);
        setMinutes(minutes);

        setClock(new SimulationClock());
        ConfigureActorsAndItems();
    }

    public void Start()
    {
        while (this.clock.getMinute() <= this.minutes)
        {
            UpdateItems();
            UpdateActors();

            this.clock.Tick();
        }
    }

    public void GenerateReports()
    {
        var configurationReport = GenerateConfigurationReport();

        var eventsReport = GenerateEventsReport();

        var actionsReport = GenerateActionsReport();

        var consumptionReport = GenerateConsumptionsReport();

        ObjectMapper mapper = new ObjectMapper();

        try
        {
            List fileNames = Arrays.asList(new File(Utils.TARGET_FOLDER).list());
            if (!fileNames.contains(Utils.REPORTS_FOLDER_NAME)) { new File(Utils.REPORTS_PATH).mkdir(); }
            new File(Utils.REPORTS_PATH + this.name + "/").mkdir();

            mapper.writeValue(new File(Utils.REPORTS_PATH + this.name + "/" + "configuration" + Utils.JSON_TYPE), configurationReport);
            mapper.writeValue(new File(Utils.REPORTS_PATH + this.name + "/" + "events" + Utils.JSON_TYPE), eventsReport);
            mapper.writeValue(new File(Utils.REPORTS_PATH + this.name + "/" + "actions" + Utils.JSON_TYPE), actionsReport);
            mapper.writeValue(new File(Utils.REPORTS_PATH + this.name + "/" + "consumptions" + Utils.JSON_TYPE), consumptionReport);
        }
        catch (IOException ex) { ex.printStackTrace(); }
    }

    public void PrintAlerts()
    {
        for(var alert : Alert.getAlerts()) { System.out.println(alert); }
    }

    public void PrintEvents()
    {
        System.out.println();
        System.out.println("[EVENTS]");

        for(var item : this.items)
        {
            for(var event : item.getEvents())
            {
                System.out.println
                (
                    "[" + event.getMinute() + "]" +
                    "[" + event.getSource() + "]" +
                    " => " +
                    "[" + event.getType() + "]" +
                    ", " +
                    "Handlers:" +
                    "[" + String.join(",", event.getHandlers()) + "]"
                );
            }
        }
        System.out.println();
    }

    public void PrintActions()
    {
        System.out.println("[ACTIONS]");
        for(var actor : this.actors)
        {
            for(var action : actor.getActions())
            {
                System.out.println
                (
                    "[" + action.getMinute() + "]" +
                    "[" + action.getActor() + "]" +
                    " => " +
                    "[" + action.getItem() + "]" +
                    " => " +
                    "[" + action.getType() + "]"
                );
            }

            System.out.println();
        }
    }

    /**
     * Registers items to actors,
     * Subscribes actors to items,
     * Sets simulation clock for items and actors.
     */
    private void ConfigureActorsAndItems()
    {
        var actorsByEventType = new HashMap<EventType, List<Actor>>();

        for(var actor : this.actors)
        {
            for(var eventType : actor.GetSupportedEventTypes())
            {
                if (!actorsByEventType.containsKey(eventType))
                {
                    actorsByEventType.put(eventType, new ArrayList<Actor>());
                }

                actorsByEventType .get(eventType).add(actor);
            }

            actor.Register(this.items);
            actor.setClock(this.clock);
        }

        for(var item : this.items)
        {
            for(var eventType : item.GetPossibleEventTypes())
            {
                var actor = actorsByEventType.get(eventType);
                if(actor == null) { continue; }
                item.Subscribe(eventType, actorsByEventType.get(eventType));
            }

            item.setClock(this.clock);
        }
    }

    private void UpdateItems()
    {
        for(var item : this.items)
        {
            item.Update();
        }
    }

    private void UpdateActors()
    {
        for(var actor : this.actors)
        {
            actor.Update();
        }
    }

    private HashMap<String,HashMap<Integer, HashMap<Room, List<String>>>> GenerateConfigurationReport()
    {
        var configurationReport = new HashMap<String,HashMap<Integer, HashMap<Room, List<String>>>>();

        for(var item : this.items)
        {
            if(!configurationReport.containsKey(this.getName())) { configurationReport.put(this.getName(), new HashMap<Integer, HashMap<Room, List<String>>>()); }
            if(!configurationReport
                    .get(this.getName())
                    .containsKey(item.getFloor()))
            {
                configurationReport
                        .get(this.getName())
                        .put(item.getFloor(), new HashMap<Room, List<String>>());
            }
            if(!configurationReport
                    .get(this.getName())
                    .get(item.getFloor())
                    .containsKey(item.getRoom()))
            {
                configurationReport
                        .get(this.getName())
                        .get(item.getFloor())
                        .put(item.getRoom(), new ArrayList<String>());
            }
            if(!configurationReport
                    .get(this.getName())
                    .get(item.getFloor())
                    .get(item.getRoom())
                    .contains(item.getName()))
            {
                configurationReport.get(this.getName())
                        .get(item.getFloor())
                        .get(item.getRoom())
                        .add(item.getName());
            }
        }
        return configurationReport;
    }

    private HashMap<EventType, HashMap<String, List<EventReport>>> GenerateEventsReport()
    {
        var eventsReport = new HashMap<EventType, HashMap<String, List<EventReport>>>();

        for(var item : this.items)
        {
            for(var event : item.getEvents())
            {
                if (!eventsReport.containsKey(event.getType()))
                {
                    eventsReport.put(event.getType(), new HashMap<String, List<EventReport>>());
                }

                if (!eventsReport.get(event.getType()).containsKey(item.getName()))
                {
                    eventsReport.get(event.getType()).put(item.getName(), new ArrayList<EventReport>());
                }

                eventsReport.get(event.getType()).get(item.getName()).add(new EventReport(event.getMinute(), event.getHandlers()));
            }
        }

        return eventsReport;
    }

    private HashMap<String, HashMap<String, Integer>> GenerateActionsReport()
    {
        var actionsReport = new HashMap<String, HashMap<String, Integer>>();

        for(var actor : this.actors)
        {
            actionsReport.put(actor.getName(), new HashMap<String,Integer>());

            for(var action : actor.getActions())
            {
                if (!actionsReport.get(actor.getName()).containsKey(action.getItem()))
                {
                    actionsReport.get(actor.getName()).put(action.getItem(), 0);
                }

                int count = actionsReport.get(actor.getName()).get(action.getItem());
                actionsReport.get(actor.getName()).put(action.getItem(), ++count);
            }
        }

        return actionsReport;
    }

    private HashMap<String, ConsumptionReport> GenerateConsumptionsReport()
    {
        var consumptionReport = new HashMap<String, ConsumptionReport>();

        for(var item :this.items)
        {
            var consumableValue = new HashMap<ConsumableType,Double>();
            var consumableCost = new HashMap<ConsumableType,Double>();

            double cost = 0.0;
            ConsumableType type = ConsumableType.Electricity;

            if(item.getConsumables().size() == 0) { continue; }

            for(var set : item.getConsumables().entrySet())
            {
                if(set.getKey().equals(ConsumableType.Electricity))
                {
                    double kiloWatts = set.getValue()/1000;
                    cost = kiloWatts * Utils.ELECTRICITY_COST;

                    type = ConsumableType.Electricity;
                }
                else if(set.getKey().equals(ConsumableType.Gas))
                {
                    double gasCubicMeter = set.getValue()/1000;
                    cost = gasCubicMeter * Utils.GAS_COST;

                    type = ConsumableType.Gas;
                }
                else if(set.getKey().equals(ConsumableType.Water))
                {
                    double meterCubic = set.getValue()/1000;
                    cost = meterCubic * Utils.WATER_COST;

                    type = ConsumableType.Water;
                }

                consumableCost.put(type, Double.parseDouble(String.format("%.2f", cost)));
                consumableValue.put(type, Double.parseDouble(String.format("%.2f", set.getValue())));
                consumptionReport.put(item.getName(), new ConsumptionReport(item.getTotalOccupationTime(),consumableValue , consumableCost));
            }
        }
        return consumptionReport;
    }

    //region getters and setters
    public String getName() { return name; }
    private void setName(String name) { this.name = name; }

    public int getMinutes() { return minutes; }
    public void setMinutes(int minutes) { this.minutes = minutes; }

    public SimulationClock getClock() { return clock; }
    public void setClock(SimulationClock clock) { this.clock = clock; }

    public List<Actor> getActors() { return actors; }
    public void setActors(List<Actor> actors) { this.actors = actors; }

    public List<Item> getItems() { return items; }
    public void setItems(List<Item> items) { this.items = items; }
    //endregion
}
