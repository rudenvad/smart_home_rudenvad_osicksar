package home_simulation.enums;

/**
 * Enum of actor state
 */
public enum ActorState
{
    Waiting,
    UsingItem,
    UsingSportItem
}
