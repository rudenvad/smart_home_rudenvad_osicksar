package home_simulation.enums;

/**
 * Enum of consumable types
 */
public enum ConsumableType
{
    Electricity,
    Water,
    Gas
}
