package home_simulation.enums;

/**
 * Enum of all possible action types
 */
public enum ActionType
{
    Occupy,
    Unoccupy,

    Repair,

    TurnOff,
    TurnOn,

    Open,
    Close,

    ClockClick,

    LightSetTurnOffTimer,

    TVVolumeUp,
    TVVolumeDown,
    TVChangeChannel,

    FridgeInsertFood,
    FridgeTakeOutFood,
    FridgeStareAt,
    FridgeTakeOutExpiredFood,

    WashingMachineInsertClothes,
    WashingMachineTakeOutClothes,
    WashingMachineStareAt,
    WashingMachineStart,

    OvenStareAt,
    OvenStart,
    OvenInsertFood,
    OvenTakeOutFood,

    PCPlayGame,
    PCUseWebBrowser,

    CameraGetRecording,
    CameraClearMemory,

    PetToStroke,
    PetFeed,

    ChairSitDown,
    ChairClear
}
