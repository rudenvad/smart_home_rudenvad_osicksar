package home_simulation.enums;

/**
 * Enum of all rooms
 */
public enum Room
{
    Attic,
    Kitchen,
    Laundry,
    Bathroom,
    Office,
    Bedroom,
    Livingroom,
    Garage
}
