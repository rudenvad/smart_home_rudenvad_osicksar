package home_simulation.enums;

/**
 * Enum of all possible event types
 */
public enum EventType
{
    Broken,

    ClockAlarm,

    LightTurnedOff,

    TVCartoonAlarm,

    FridgeCloseAlarm,
    FridgeFoodExpired,
    FridgeIsEmpty,

    WashingFinished,

    OvenWorkingFinished,

    ReceivedEmailNotification,

    StrangeActivityNotification,
    CameraMemoryIsFull,

    PetIsHungry,

    ChairIsDirty
}
