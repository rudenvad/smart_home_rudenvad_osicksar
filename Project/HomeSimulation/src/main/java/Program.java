import home_simulation.actors.*;
import home_simulation.actors.abstrct.Actor;
import home_simulation.items.abstrct.Item;
import home_simulation.simulation.SimulationFactory;
import home_simulation.Utils.Utils;
import home_simulation.enums.Room;
import home_simulation.items.*;
import home_simulation.items.pets.Cat;
import home_simulation.items.sport.Bike;
import home_simulation.items.sport.Ski;

import java.util.ArrayList;

public class Program
{
    public static void main(String[] args)
    {
        //using already manually configured simulation created with factory
        var firstSimulation = Utils.FirstManualSimulation();
        firstSimulation.Start();

        //Prints alerts to console
        firstSimulation.PrintAlerts();

        //Prints events to console
//        firstSimulation.PrintEvents();

        //Prints actions to console
//        firstSimulation.PrintActions();

        firstSimulation.GenerateReports();

//        using already manually configured simulation created with factory
        var secondSimulation = Utils.SecondManualSimulation();
        secondSimulation.Start();

        //Prints alerts to console
        secondSimulation.PrintAlerts();

        //Prints events to console
//        secondSimulation.PrintEvents();

        //Prints actions to console
//        secondSimulation.PrintActions();

        secondSimulation.GenerateReports();
    }

    //Example method, 3 ways how to configure simulation.
    //Manually, by parameters with factory, random with factory
    private void ExampleCreatingSimulation()
    {
        var simulationName = "Test Simulation";
        var simulationMinutes = 60;//*24;

        var grandFather = new GrandFather("GrandFather");
        var grandMother = new GrandMother("GrandMother");
        var mother = new Mother("Mother");
        var father = new Father("Father");
        var middleChild = new MiddleChild("MiddleChild");
        var youngChild =  new JuniorChild("YoungChild");
        var actors = new ArrayList<Actor>();
        actors.add(grandFather);
        actors.add(grandMother);
        actors.add(mother);
        actors.add(father);
        actors.add(middleChild);
        actors.add(youngChild);

        var bigClock = new Clock("Big Clock", Room.Livingroom,1);
        var light = new Light(Room.Bedroom + "Light", Room.Bedroom, 1);
        var tv = new TV(Room.Bedroom + "TV", Room.Bedroom, 1);
        var washingMachine = new WashingMachine(Room.Laundry + "WashingMachine", Room.Laundry, 0);
        var fridge = new Fridge(Room.Kitchen + "Fridge", Room.Kitchen, 0);
        var oven = new Oven(Room.Kitchen+"Oven",Room.Kitchen,0);
        var pc = new PC(Room.Office + "PC", Room.Office, 2);
        var securityCamera = new SecurityCamera(Room.Office + "Camera", Room.Office,2);

        //pets
        var cat1 = new Cat("Cat1", Room.Kitchen, 0);
        var cat2 = new Cat("Cat2", Room.Kitchen, 0);
        var cat3 = new Cat("Cat3", Room.Kitchen, 0);

        //Sport 2 bikes and 1 ski
        var bike = new Bike("Bike", Room.Garage, 0);
        var oldBike = new Bike("Old Bike", Room.Garage, 0);
        var ski = new Ski("Ski 2000", Room.Garage, 0);

        var items = new ArrayList<Item>();
        items.add(bigClock);
        items.add(light);
        items.add(tv);
        items.add(fridge);
        items.add(washingMachine);
        items.add(oven);
        items.add(pc);
        items.add(securityCamera);

        items.add(cat1);
        items.add(cat2);
        items.add(cat3);

        items.add(oldBike);
        items.add(bike);
        items.add(ski);

        for(int i = 0; i < 12; i++)
        {
            items.add(new Light(i + "-Light", Room.Attic, 3));
        }

        //creating simulation from parameters with factory.
        var simulation = SimulationFactory.Instance().CreateSimulation(simulationName, simulationMinutes, actors, items);

        //creating simulation with pseudo random parameters with factory
//        var simulation = SimulationFactory.Instance().CreateRandomSimulation();

        //creating simulation manually.
//        var simulation = new Simulation(simulationName, simulationMinutes, actors, items);
    }
}
